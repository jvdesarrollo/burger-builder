import axios from "axios";

const instance = axios.create({
  baseURL: "https://my-burger-2-a1716.firebaseio.com/",
});

export default instance;
